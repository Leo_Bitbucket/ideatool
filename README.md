# idea-tool

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

#############################################################
Database:

1. create a new column in Ideas.
 tags: Array
2. create a new class: HashTags
3. create a new column in HashTags: String: name.
4. Add all tags into the database.
