'use strict';

angular.module('ideaToolApp')
	.factory('dataService', [
		'$http',
		function($http) {

			return {

				getConfig: function(){
					var promise = $http({
						method: 'GET',
						url: 'config/config.json'
					}).success(function(data) {
						return data;
					});
					return promise;
				}
			};
		}
	]);
