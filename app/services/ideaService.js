'use strict';

angular.module('ideaToolApp')
  .factory('ideaService', [
    '$http',
    '$q',
    'dataService',
    'ENV',
    function ($http, $q, dataService, ENV) {
    var config;
    return {

      setConfig: function(data){
        this.config = data;
      },

      getAllIdeas:function(skip){
        var PARSE_HEADER = {
          'Content-Type': 'application/json'
        };
        PARSE_HEADER = $.extend(PARSE_HEADER, this.config.data.settings[ENV.name].header);
        if (skip) {
          var req = {
            method:'GET',
            headers:PARSE_HEADER,
            url:'https://api.parse.com/1/classes/Ideas',
            params: {
                'limit': 1000,
                'order': '-createdAt',
                'where': {"createdAt":{"$lt":skip}, "type":"user"}
            }
          };
        } else {
          var req = {
            method:'GET',
            headers:PARSE_HEADER,
            url:'https://api.parse.com/1/classes/Ideas',
            params: {
                'limit': 1000,
                'order': '-createdAt',
                'where': {"type":"user"}
            }
          };
        }
        return $q(function(resolve,reject){
          $http(req).success(function(data, status, headers, config){
            resolve(data);
          }).error(function(err){
            reject(err);
          })
        });
      },

      updateIdeaTags: function(ideaId, ideaTags){
        var PARSE_HEADER = {
          'Content-Type': 'application/json'
        };
        PARSE_HEADER = $.extend(PARSE_HEADER, this.config.data.settings[ENV.name].header);
        var req = {
          method:'PUT',
          headers:PARSE_HEADER,
          url:'https://api.parse.com/1/classes/Ideas/' + ideaId,
          data: {
              'tags': ideaTags
          }
        };
        return $q(function(resolve,reject){
          $http(req).success(function(data){
            resolve(data);
          }).error(function(err){
            reject(err);
          })
        });
      },

      getAllTags: function(){
        var PARSE_HEADER = {
          'Content-Type': 'application/json'
        };
        PARSE_HEADER = $.extend(PARSE_HEADER, this.config.data.settings[ENV.name].header);
        var req = {
          method:'GET',
          headers:PARSE_HEADER,
          url:'https://api.parse.com/1/classes/HashTags'
        };
        return $q(function(resolve,reject){
          $http(req).success(function(data){
            resolve(data);
          }).error(function(err){
            reject(err);
          })
        });
      }

    };
  }]);
