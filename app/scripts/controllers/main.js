'use strict';

/**
 * @ngdoc function
 * @name ideaToolApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ideaToolApp
 */
angular.module('ideaToolApp')
  .controller('MainCtrl', [
    '$scope',
    'ideaService',
    '$uibModal',
    'dataService',
    function ($scope, ideaService, $uibModal, dataService) {
      $scope.loadingIdeas = true;
      /* get all results */
      var results = [];

      var queryIdeas = function(skip){
        ideaService.getAllIdeas(skip).then(function(data){
          queryCallback(data)
        });
      }

      var queryCallback = function(data){
        results.push.apply(results, data.results);
        if (data.results.length == 1000) {
          queryIdeas(data.results[data.results.length - 1].createdAt);
        }else {
          $scope.results = results;
          updateFilteredItems();
          $scope.loadingIdeas = false;
        }
      }

      dataService.getConfig().then(function(server_data){
        ideaService.setConfig(server_data);
        queryIdeas(false);

        /* get all tags */
        $scope.ideaTags = [];
        ideaService.getAllTags().then(function(data){
          for (var i = 0; i < data.results.length; i++) {
            $scope.ideaTags.push({'name': data.results[i].name, 'selected':false});
          }
        });
      });

      /* pagination */
      $scope.pagedResults = [];
      $scope.currentPage = 1;
      $scope.maxSize = 5;
      $scope.itemsPerPage = 10;

      $scope.$watch('currentPage + itemsPerPage', updateFilteredItems);

      function updateFilteredItems() {
        var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
        var end = begin + $scope.itemsPerPage;
        $scope.pagedResults = results.slice(begin, end);
        $scope.rowCollection = results.slice(begin, end);
      }

      $scope.open = function (idea) {
        var modalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'myModalContent.html',
          controller: 'ModalInstanceCtrl',
          size: 'sm',
          resolve: {
            ideaTags: function () {
              return $scope.ideaTags;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          //confirm
          var selectedTags = [];
          for (var i = 0; i < selectedItem.length; i++) {
            if (selectedItem[i].selected) {
              selectedTags.push(selectedItem[i].name);
            }
          }
          if (selectedTags.length > 0) {
            ideaService.updateIdeaTags(idea.objectId, selectedTags).then(function(){
              idea.tags = selectedTags;
            });
          }
          for (var i = 0; i < $scope.ideaTags.length; i++) {
            $scope.ideaTags[i].selected = false;
          }
        }, function () {
          //cancel
          for (var i = 0; i < $scope.ideaTags.length; i++) {
            $scope.ideaTags[i].selected = false;
          }
        });
      };

      $scope.displayImage = function(url){
        var modalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'imageModalPage.html',
          controller: function ($scope, $uibModalInstance) {
              $scope.imageURL = url;
          },
          windowClass: 'large-modal'
        });
      }

      $scope.searchIdeas = function(){
        if (typeof $scope.keyword == 'undefined' || $scope.keyword == '') {
          return;
        }
        var searchResults = [];
        for (var i = 0; i < results.length; i++) {
          if (results[i].objectId == $scope.keyword ||
              results[i].description == $scope.keyword ){
            searchResults.push(results[i]);
          } else if(results[i].tags != "undefined" && (results[i].tags instanceof Array)){
            if (results[i].tags.indexOf($scope.keyword) > -1) {
              searchResults.push(results[i]);
            }
          }
        }
        if (searchResults.length > 0) {
          $scope.pagedResults = searchResults;
        } else {
          $scope.pagedResults = [];
        }
      }

  }]);
