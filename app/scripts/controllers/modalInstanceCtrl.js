'use strict';

angular.module('ideaToolApp')
  .controller('ModalInstanceCtrl', [
    '$scope',
    '$uibModalInstance',
    'ideaTags',
    function ($scope, $uibModalInstance, ideaTags) {
    $scope.ideaTags = ideaTags;
      $scope.ok = function () {
        $uibModalInstance.close($scope.ideaTags);
      };

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
  }]);
