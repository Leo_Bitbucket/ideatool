'use strict';

/**
 * @ngdoc overview
 * @name ideaToolApp
 * @description
 * # ideaToolApp
 *
 * Main module of the application.
 */
angular
  .module('ideaToolApp', [
    'environment',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'smart-table',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
